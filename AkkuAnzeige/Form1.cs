﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace WindowsFormsApplication1
{
    public partial class Form1 : Form
    {
        float warningAt = 0.2F;
        bool messageDisplayed = false;
        //to only display a message when we DRAIN battery
        //float batteryBefore = 0;

        public Form1()
        {
            //batteryBefore = SystemInformation.PowerStatus.BatteryLifePercent;

            InitializeComponent();
            CheckBattery();

            Timer t1 = new Timer();
            t1.Interval = 1000;
            t1.Tick += (sender, e) => { CheckBattery(); };
            t1.Start(); 

        }

        public void CheckBattery()
        {
            bool isCharging = IsCharging();
            CheckStatus(isCharging);
            ControlChargingStatus.Visible = isCharging;
        }

        private void CheckStatus(bool isCharging)
        {
            float battery = SystemInformation.PowerStatus.BatteryLifePercent;

            if (battery <= warningAt)
            {
                BackColor = Color.Red;

                //warning only when draining and exactly at warning
                if (!IsCharging() && battery == warningAt && !messageDisplayed)
                {
                    //batteryBefore = battery;
                    messageDisplayed = true;
                    MessageBox.Show($"You have less than {warningAt} battery left!", "Battery warning");
                }
            }
            else
            {
                messageDisplayed = false;

                if (battery < 0.3 && battery > 0.16)
                {
                    BackColor = Color.Orange;
                    NotifyIcon.Icon = Properties.Resources.iconBatteryLow;
                    Icon = Properties.Resources.iconBatteryLow;
                }
                else if (battery < 0.7 && battery >= 0.3)
                {
                    BackColor = Color.YellowGreen;
                    NotifyIcon.Icon = Properties.Resources.iconBatteryMid;
                    Icon = Properties.Resources.iconBatteryMid;
                }
                else if (battery >= 0.7)
                {
                    BackColor = Color.LightGreen;
                    NotifyIcon.Icon = Properties.Resources.iconBatteryFull;
                    Icon = Properties.Resources.iconBatteryFull;
                }
            }

            //display battery percentage
            int batteryPercentage = (int)(100 * battery);
            string batteryText = batteryPercentage.ToString() + "%";
            akkuAnzeige.Text = batteryText;

            NotifyIcon.Text = batteryText + (isCharging ? " (Charging)" : "");
        }

        private bool IsCharging()
        {
            return PowerLineStatus.Online == SystemInformation.PowerStatus.PowerLineStatus;
            //int charging = (int)SystemInformation.PowerStatus.PowerLineStatus;
            //return charging.Equals(1);
        }

        private void Form1_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyData.Equals(Keys.F))
                TopMost = !TopMost;
        }

        private void Form1_Resize(object sender, EventArgs e)
        {
            //warningAt = (float)ControlWarningAt.Value / 100F;
            if (WindowState == FormWindowState.Minimized)
            {
                Hide();
                NotifyIcon.Visible = true;
            }
        }

        private void NotifyIcon_Click(object sender, EventArgs e)
        {
            Show();
            WindowState = FormWindowState.Normal;
            NotifyIcon.Visible = false;
        }

        #region mouse movement logic
        bool mouseDown;
        Point lastLocation;

        private void Form1_MouseDown(object sender, MouseEventArgs e)
        {
            mouseDown = true;
            lastLocation = e.Location;
        }

        private void Form1_MouseMove(object sender, MouseEventArgs e)
        {
            if (mouseDown)
            {
                Location = new Point(
                    (Location.X - lastLocation.X) + e.X, (Location.Y - lastLocation.Y) + e.Y);

                Update();
            }
        }

        private void Form1_MouseUp(object sender, MouseEventArgs e)
        {
            mouseDown = false;
        }
        #endregion
    }
}
