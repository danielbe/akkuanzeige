﻿namespace WindowsFormsApplication1
{
    partial class Form1
    {
        /// <summary>
        /// Erforderliche Designervariable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Verwendete Ressourcen bereinigen.
        /// </summary>
        /// <param name="disposing">True, wenn verwaltete Ressourcen gelöscht werden sollen; andernfalls False.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Vom Windows Form-Designer generierter Code

        /// <summary>
        /// Erforderliche Methode für die Designerunterstützung.
        /// Der Inhalt der Methode darf nicht mit dem Code-Editor geändert werden.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.akkuAnzeige = new System.Windows.Forms.Label();
            this.NotifyIcon = new System.Windows.Forms.NotifyIcon(this.components);
            this.ControlChargingStatus = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.ControlChargingStatus)).BeginInit();
            this.SuspendLayout();
            // 
            // akkuAnzeige
            // 
            this.akkuAnzeige.AutoSize = true;
            this.akkuAnzeige.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.akkuAnzeige.Location = new System.Drawing.Point(12, 21);
            this.akkuAnzeige.Name = "akkuAnzeige";
            this.akkuAnzeige.Size = new System.Drawing.Size(0, 18);
            this.akkuAnzeige.TabIndex = 3;
            // 
            // NotifyIcon
            // 
            this.NotifyIcon.Icon = ((System.Drawing.Icon)(resources.GetObject("NotifyIcon.Icon")));
            this.NotifyIcon.Text = "Battery";
            this.NotifyIcon.Click += new System.EventHandler(this.NotifyIcon_Click);
            // 
            // ControlChargingStatus
            // 
            this.ControlChargingStatus.Image = global::WindowsFormsApplication1.Properties.Resources.Unbenannt;
            this.ControlChargingStatus.InitialImage = global::WindowsFormsApplication1.Properties.Resources.Unbenannt;
            this.ControlChargingStatus.Location = new System.Drawing.Point(61, 3);
            this.ControlChargingStatus.Name = "ControlChargingStatus";
            this.ControlChargingStatus.Size = new System.Drawing.Size(41, 56);
            this.ControlChargingStatus.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.ControlChargingStatus.TabIndex = 2;
            this.ControlChargingStatus.TabStop = false;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(135, 58);
            this.Controls.Add(this.akkuAnzeige);
            this.Controls.Add(this.ControlChargingStatus);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Name = "Form1";
            this.ShowIcon = false;
            this.KeyUp += new System.Windows.Forms.KeyEventHandler(this.Form1_KeyUp);
            this.MouseDown += new System.Windows.Forms.MouseEventHandler(this.Form1_MouseDown);
            this.MouseMove += new System.Windows.Forms.MouseEventHandler(this.Form1_MouseMove);
            this.MouseUp += new System.Windows.Forms.MouseEventHandler(this.Form1_MouseUp);
            this.Resize += new System.EventHandler(this.Form1_Resize);
            ((System.ComponentModel.ISupportInitialize)(this.ControlChargingStatus)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.PictureBox ControlChargingStatus;
        private System.Windows.Forms.Label akkuAnzeige;
        private System.Windows.Forms.NotifyIcon NotifyIcon;
    }
}

